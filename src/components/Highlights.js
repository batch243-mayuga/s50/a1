// import of the classes needed for the CRC rule as well as the classes needed for the bootstrap component
import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {

	return(
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				      <Card.Body>
				        <Card.Title><h2>Learn From Home</h2></Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus varius massa vitae ante lacinia, sed tincidunt nulla porta. Quisque in facilisis felis, eget pellentesque tellus. Proin eu risus id mauris rhoncus tincidunt. Aenean ut dui nisi. Nunc ipsum turpis, blandit eu ullamcorper et, lobortis vitae nisl. Ut sodales ligula ut tortor laoreet dictum quis at risus. Suspendisse id mattis turpis, ac sagittis felis. Etiam efficitur aliquam arcu id ultrices. Nam a commodo mauris, molestie fermentum leo. Aliquam erat volutpat. Nunc accumsan urna enim, non fermentum elit varius sed. Ut facilisis viverra magna, sed pharetra nibh pellentesque commodo. Aliquam erat volutpat. Aliquam malesuada velit nibh, vitae euismod nunc fringilla non. Pellentesque dictum id dolor sit amet tempus.
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>

		{/*Second Card*/}
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				      <Card.Body>
				        <Card.Title><h2>Study Now, Pay Later</h2></Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus varius massa vitae ante lacinia, sed tincidunt nulla porta. Quisque in facilisis felis, eget pellentesque tellus. Proin eu risus id mauris rhoncus tincidunt. Aenean ut dui nisi. Nunc ipsum turpis, blandit eu ullamcorper et, lobortis vitae nisl. Ut sodales ligula ut tortor laoreet dictum quis at risus. Suspendisse id mattis turpis, ac sagittis felis. Etiam efficitur aliquam arcu id ultrices. Nam a commodo mauris, molestie fermentum leo. Aliquam erat volutpat. Nunc accumsan urna enim, non fermentum elit varius sed. Ut facilisis viverra magna, sed pharetra nibh pellentesque commodo. Aliquam erat volutpat. Aliquam malesuada velit nibh, vitae euismod nunc fringilla non. Pellentesque dictum id dolor sit amet tempus.
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>

		{/*Third Card*/}
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				      <Card.Body>
				        <Card.Title><h2>Be Part of our Community</h2></Card.Title>
				        <Card.Text>
				          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus varius massa vitae ante lacinia, sed tincidunt nulla porta. Quisque in facilisis felis, eget pellentesque tellus. Proin eu risus id mauris rhoncus tincidunt. Aenean ut dui nisi. Nunc ipsum turpis, blandit eu ullamcorper et, lobortis vitae nisl. Ut sodales ligula ut tortor laoreet dictum quis at risus. Suspendisse id mattis turpis, ac sagittis felis. Etiam efficitur aliquam arcu id ultrices. Nam a commodo mauris, molestie fermentum leo. Aliquam erat volutpat. Nunc accumsan urna enim, non fermentum elit varius sed. Ut facilisis viverra magna, sed pharetra nibh pellentesque commodo. Aliquam erat volutpat. Aliquam malesuada velit nibh, vitae euismod nunc fringilla non. Pellentesque dictum id dolor sit amet tempus.
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>

		</Row>
		)
}