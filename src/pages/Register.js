import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { Container, Row, Col } from 'react-bootstrap';

import { useNavigate } from 'react-router-dom';

import { useState, useEffect } from 'react';

// import UserContext from '../UserContext';

import Swal from 'sweetalert2'

export default function Register() {
	// State hooks to store the values of the input field from our user
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive]  = useState(false);
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	const navigate = useNavigate();

	// const {user, setUser} = useContext(UserContext);

	// Business Logic
	// We want to disable the register button if one of the input fields is empty


	useEffect(()=> {
		// console.log(email);
		// console.log(password1);
		// console.log(password2);
		if(firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !== '' && password1 === password2){
				setIsActive(true);
		}else{
			setIsActive(false);
		}

	}, [firstName, lastName, email, mobileNo, password1, password2])

	// This function will be triggered when the inputs in the form will be submitted

	function registerUser (event){
		event.preventDefault()

		/*alert(`Congratulations ${email}, you are now registered in our website!`)

		localStorage.setItem('email', email);

		setUser(localStorage.getItem('email'))

		setEmail('');
		setPassword1('');
		setPassword2('');*/

		fetch(`${process.env.REACT_APP_URI}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				firstName,
				lastName,
				email,
				mobileNo,
				password: password1
				
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			if(data.emailAlreadyExists){
				Swal.fire({
					title: 'Duplicate email found',
					icon: 'error',
					text: 'Please provide a different email.'
				})

				setPassword1('');
				setPassword2('');
			} else if(data.mobileNoLength === false){
				Swal.fire({
					title: 'Invalid Mobile Number',
					icon: 'error',
					text: 'Mobile number must be atleast 11 digits!'
				})

				setPassword1('');
				setPassword2('');
			} else{
				Swal.fire({
					title: "Registration successful",
					icon: 'success',
					text: 'Welcome to Zuitt'
				})
				 navigate('/login');
			}
		})
	}


	return(
	<Container>
		<Row>
			<Col className= "col-md-4 col-8 offset-md-4 offset-2">
				<Form onSubmit={registerUser} className="bg-secondary p-3">

					  <Form.Group className="mb-3" controlId="firstName">
					    <Form.Label>First Name:</Form.Label>
					    <Form.Control 
					    	type="text" 
					    	placeholder="Enter first name" 
					    	value={firstName}
					    	onChange={event => setFirstName(event.target.value
					    		)}
					    	required />
					  </Form.Group>

				      <Form.Group className="mb-3" controlId="lastName">
				        <Form.Label>Last Name:</Form.Label>
				        <Form.Control 
				        	type="text" 
				        	placeholder="Enter last name" 
				        	value={lastName}
				        	onChange={event => setLastName(event.target.value
				        		)}
				        	required />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="email">
				        <Form.Label>Email:</Form.Label>
				        <Form.Control 
				        	type="email" 
				        	placeholder="Enter email" 
				        	value={email}
				        	onChange={event => setEmail(event.target.value
				        		)}
				        	required />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="mobileNo">
				        <Form.Label>Mobile Number:</Form.Label>
				        <Form.Control 
				        	type="number" 
				        	placeholder="Enter mobile number" 
				        	value={mobileNo}
				        	onChange={event => setMobileNo(event.target.value
				        		)}
				        	required />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="password1" >
				        <Form.Label>Enter your desired Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	placeholder="Password"
				        	value={password1}
				        	onChange={event => setPassword1(event.target.value)}
				        	required />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="password2">
				        <Form.Label>Verify your Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	placeholder="Password"
				        	value={password2}
				        	onChange={event => setPassword2(event.target.value)}
				        	required />
				      </Form.Group>
				      
				      <Button variant="primary" type="submit" disabled={!isActive}>
				        Register
				      </Button>
				    </Form>
			</Col>
		</Row>
	</Container>
		)
}