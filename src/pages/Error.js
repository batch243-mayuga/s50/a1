
export default function Error(){

	return(
		<>
		<h1>PAGE NOT FOUND</h1>
		<p>Go back to the <a href="/">homepage</a></p>

		</>
		)
}