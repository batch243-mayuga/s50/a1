import { Navigate } from 'react-router-dom'
import { useContext, useEffect } from 'react'

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Logout(){

	const {setUser, unSetUser} = useContext(UserContext);

	unSetUser();

	useEffect(()=> {
		setUser({id: null, isAdmin: false})
	}, [])

	Swal.fire({
		title: 'Welcome back to the outside world!',
		icon: 'info',
		text: 'Babalik ka rin!'
	})

	// setUser(localStorage.getItem('email'));

	// localStorage.clear()

	return(
		<Navigate to="/login" />
		)
}