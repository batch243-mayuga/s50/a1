// import { Container } from 'react-bootstrap';
import CourseCard from '../components/CourseCard';
// import coursesData from '../data/courses';
import { Fragment, useEffect, useState } from 'react';

export default function Courses() {

	const [courses, setCourses] = useState([]);
	// console.log(coursesData);

	// Syntax:
		// localStorage.getItem('propertyName');

	// const local = localStorage.getItem("email")
	// console.log(local);

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_URI}/courses/allActiveCourses`)
		.then(response => response.json())
		.then(data => {
			console.log(data)

			// Sets the 'courses' state to map the data retrieved from the fetch request into several 'CourseCard' component
			setCourses(data.map(course => {
		return(
				<CourseCard key={course._id} coursesProp ={course}/>
			)
	}))
		})
	}, [])

	

	return(
		<Fragment>
			{courses}
		</Fragment>
		)
}