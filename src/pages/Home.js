import { Container } from 'react-bootstrap'
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'


export default function Home() {

	return(
		<Container>
		  <Banner />
		  <Highlights />
		  
		</Container>
		)
}